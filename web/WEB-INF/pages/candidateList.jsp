<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Candidates</title>
</head>
<body>
<div>Candidates</div>
<table border="1">
  <tr>
    <th>Name</th>
    <th>Actions</th>
  </tr>
  <s:iterator value="candidates">
    <tr>
      <td><s:property value="name"/></td>
      <td>&nbsp;</td>
    </tr>
  </s:iterator>
</table>
</body>
</html>
