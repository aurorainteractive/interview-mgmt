package interviewmgmt.action;

import java.util.Collection;

import com.opensymphony.xwork2.ActionSupport;
import interviewmgmt.service.InterviewMgmtService;
import interviewmgmt.service.data.CandidateData;

/**
 */
public class ListCandidatesAction extends ActionSupport
{
   private InterviewMgmtService interviewMgmtService;

   private Collection<CandidateData> candidates;

   public String execute() throws Exception
   {
      candidates = interviewMgmtService.listCandidates();

      return SUCCESS;
   }

   public void setInterviewMgmtService(InterviewMgmtService interviewMgmtService)
   {
      this.interviewMgmtService = interviewMgmtService;
   }

   public Collection<CandidateData> getCandidates()
   {
      return candidates;
   }
}
