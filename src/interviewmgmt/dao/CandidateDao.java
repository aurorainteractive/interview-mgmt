package interviewmgmt.dao;

import java.util.Collection;

import interviewmgmt.dto.Candidate;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 */
@Transactional
public class CandidateDao
{
   private SessionFactory sessionFactory;

   public void setSessionFactory(SessionFactory sessionFactory)
   {
      this.sessionFactory = sessionFactory;
   }

   public Collection<Candidate> listCandidates()
   {
      //noinspection unchecked
      return sessionFactory.getCurrentSession().createQuery("from Candidate c").list();
   }
}
