package interviewmgmt.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import interviewmgmt.dao.CandidateDao;
import interviewmgmt.dto.Candidate;
import interviewmgmt.service.data.CandidateData;

/**
 */
public class InterviewMgmtServiceImpl implements InterviewMgmtService
{
   private CandidateDao candidateDao;

   @Override
   public Collection<CandidateData> listCandidates()
   {
      Collection<Candidate> candidates = candidateDao.listCandidates();
      List<CandidateData> candidateDatas = new ArrayList<>();

      for(Candidate candidate : candidates)
      {
         CandidateData candidateData = new CandidateData();
         candidateData.setId(candidate.getId());
         candidateData.setName(candidate.getName());
         candidateDatas.add(candidateData);
      }

      return candidateDatas;
   }

   public void setCandidateDao(CandidateDao candidateDao)
   {
      this.candidateDao = candidateDao;
   }
}
