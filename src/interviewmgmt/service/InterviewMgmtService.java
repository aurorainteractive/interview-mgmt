package interviewmgmt.service;

import java.util.Collection;

import interviewmgmt.service.data.CandidateData;

/**
 */
public interface InterviewMgmtService
{
   Collection<CandidateData> listCandidates();
}
